import snap
import random
import itertools
#import matplotlib.pyplot as plt


def initialize(cq=2, cu=2):
    B = snap.TNEANet.New()
    G = snap.TNEANet.New()
    # add a number of nodes and a number of communities
    nodes = [i for i in xrange(1, 101)]
    communities = [-i for i in xrange(2, 22)]  # starting from -2 because snap already uses -1
    for node in nodes:
        B.AddNode(node)
        G.AddNode(node)
    for community in communities:
        B.AddNode(community)
    # for all nodes add cq edges (random sampling without replacement)
    for node in nodes:
        sample = random.sample(communities, cq)
        for community in sample:
            B.AddEdge(node, community)
    # for all communities add cu edges (random sampling without replacement)
    for community in communities:
        sample = random.sample(nodes, cu)
        for node in sample:
            if not B.IsEdge(node, community):  # handle B as a simple graph
                B.AddEdge(node, community)
    # for all node combinations add as many edges between them as there are communities that they share
    for a, b in itertools.combinations(nodes, 2):
        number_of_shared_communities = len(set([comm for comm in B.GetNI(a).GetOutEdges()]).intersection(set([comm for comm in B.GetNI(b).GetOutEdges()])))
        while number_of_shared_communities > 0:
            G.AddEdge(a, b)
            G.AddEdge(b, a)
            number_of_shared_communities -= 1
    return B, G, nodes, communities


def evolution_of_q(B, G, nodes, cq=2, s=2):
    # to pick a node as prototype for a new node, with probability proportional to its degree
    # we simply pick a random edge linking a node to a community and use this node
    prototype = get_node(B.GetEI(random.choice(xrange(0, B.GetEdges()))))
    # the new node is assigned the next integer, and is added to the nodes list and the two graphs
    q = len(nodes) + 1
    nodes.append(q)
    B.AddNode(q)
    G.AddNode(q)
    # cq linked communities of prototype are chosen uniformly at random (without replacement)
    edges_to_be_copied = random.sample([n for n in B.GetNI(prototype).GetOutEdges()], cq)
    # we add every occurrence of a node in the chosen communities in a list
    neighbors = [neighbor for comm in edges_to_be_copied for neighbor in [n for n in B.GetNI(comm).GetInEdges()]]

    # preferentially chosen edges
    # s nodes are chosen independently of each other with probability proportional to their degrees
    s_nodes = [G.GetEI(random.choice(xrange(0, G.GetEdges()))).GetSrcNId() for _ in itertools.repeat(None, s)]

    # the chosen communities are linked with q
    for edge in edges_to_be_copied:
        B.AddEdge(q, edge)
    # for every occurrence of a node in the communities of q we add an edge linking them
    for neighbor in neighbors:
        G.AddEdge(neighbor, q)
        G.AddEdge(q, neighbor)
    # preferentially chosen edges

    # for every node in the s chosen nodes, we add an edge linking it with q
    for s_node in s_nodes:
        G.AddEdge(s_node, q)
        G.AddEdge(q, s_node)
    return B, G, nodes


def evolution_of_u(B, G, communities, cu = 2):
    # to pick a community as prototype for a new community, with probability proportional to its degree
    # we simply pick a random edge linking a node to a community and use this community
    prototype = get_community(B.GetEI(random.choice(xrange(0, B.GetEdges()))))
    # the new community is assigned the next integer, and is added to the communities list and B
    u = -(len(communities) + 2)
    communities.append(u)
    B.AddNode(u)
    # cu linked nodes of prototype are chosen uniformly at random (without replacement)
    edges_to_be_copied = random.sample([n for n in B.GetNI(prototype).GetInEdges()], cu)
    # the chosen nodes are linked with u
    for edge in edges_to_be_copied:
        B.AddEdge(edge, u)
    # a new added is added between q1 and q2 for all combinations of u's neighbors
    for q1, q2 in itertools.combinations(edges_to_be_copied, 2):
        G.AddEdge(q1, q2)
        G.AddEdge(q2, q1)
    return B, G, communities


def simulation(n=50, b=0.8, cq=2, cu=2, s=2):
    random.seed(7)
    B, G, nodes, communities = initialize(cq, cu)
    while n > 0:
        n -= 1
        if random.randint(1, 100) <= 100 * b:
            B, G, nodes = evolution_of_q(B, G, nodes, cq, s)
        else:
            B, G, communities = evolution_of_u(B, G, communities, cu)
        if n % 1000 == 0:
            print G.GetNodes(), G.GetEdges(), snap.GetBfsFullDiam(G, 100, True), snap.GetBfsEffDiam(G, 100, True)
        # draw graphs
        """pos = nx.shell_layout(B)
        nx.draw(B, pos, with_labels=True)
        plt.savefig(str("%03d"%(50 - n)) + "b.jpg")
        plt.clf()
        pos = nx.spring_layout(G)
        nx.draw(G, pos, with_labels=True)
        plt.savefig(str("%03d"%(50 - n)) + "g.jpg")
        plt.clf()"""
    #print G.GetNodes(), G.GetEdges(), snap.GetBfsFullDiam(G, 100), snap.GetBfsEffDiam(G, 100, True)
    CntV = snap.TIntPrV()
    snap.GetOutDegCnt(G, CntV)
    for p in CntV:
        print "degree %d: count %d" % (p.GetVal1(), p.GetVal2())

    return B, G, nodes, communities


def get_node(edge):
    if edge.GetDstNId() < 0:
        return edge.GetSrcNId()
    else:
        return edge.GetDstNId()


def get_community(edge):
    if edge.GetDstNId() < 0:
        return edge.GetDstNId()
    else:
        return edge.GetSrcNId()


B, G, nodes, communities = simulation(n=10000, b=0.8, cq=2, cu=2, s=2)
